public class Funcionario {
    private String name;
    private int func;
    private double salary;

    public Funcionario(String name, int func, double salary) {
        this.name = name;
        this.func = func;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFunc() {
        return func;
    }

    public void setFunc(int func) {
        this.func = func;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

}
