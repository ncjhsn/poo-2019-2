import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //[1] - Gerente [2] - Secretario [3] - Operario
        Scanner s = new Scanner(System.in);

        Empresa continental = new Empresa();
        ArrayList<Funcionario> ops = new ArrayList<Funcionario>();

        Funcionario g1 = new Funcionario("John Wick", 1, 150.0);
        Funcionario s1 = new Funcionario("Bowery King", 2, 150.0);
        Funcionario o1 = new Funcionario("Winston", 3, 1510.0);
        Funcionario o2 = new Funcionario("Huang", 3, 1520.0);
        Funcionario o3 = new Funcionario("Ares", 3, 1530.0);
        Funcionario o4 = new Funcionario("Charon", 3, 1540.0);
        Funcionario o5 = new Funcionario("Sofia", 3, 1550.0);
        Funcionario o6 = new Funcionario("Tick Tock Man", 3, 15600.0);

        Departamento d1 = new Departamento("Bioquimica", g1, s1, ops);
        Departamento d2 = new Departamento("Informatica", g1, s1, ops);
        Departamento d3 = new Departamento("Segurança", g1, s1, ops);
        Departamento d4 = new Departamento("Saude", g1, s1, ops);
        Departamento d5 = new Departamento("Mec", g1, s1, ops);
        Departamento d7 = new Departamento("Gurizada's crew7", g1, s1, ops);

        d1.cadastrarOperario(o1);
        d1.cadastrarOperario(o2);
        d1.cadastrarOperario(o3);
        d1.cadastrarOperario(o4);
        d1.cadastrarOperario(o5);

        continental.createDp(d1);
        continental.createDp(d2);
        continental.createDp(d3);
        continental.createDp(d4);
        continental.createDp(d5);

        System.out.println(d1.returnEmpregados());
        System.out.println("Departamento: " + continental.returnDps(d1));
        System.out.println("Departamento: " + continental.returnDps(d2));
        System.out.println("Departamento: " + continental.returnDps(d3));
        System.out.println("Departamento: " + continental.returnDps(d4));
        System.out.println("Departamento: " + continental.returnDps(d5));


    }
}
