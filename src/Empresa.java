import java.util.ArrayList;

public class Empresa {
    private ArrayList<Departamento> dps = new ArrayList<Departamento>();

    public ArrayList<Departamento> getDps() {
        return dps;
    }

    public void setDps(ArrayList<Departamento> dps) {
        this.dps = dps;
    }

    public void createDp(Departamento dp) {
        if (this.dps.size() < 5) {
            dps.add(dp);
        } else {
            throw new Error("Deu brete [Departamentos foi excedido]");
        }
    }

    public String returnDps(Departamento d){
        return d.getNome();
    }

}
