import java.util.ArrayList;

public class Departamento {
    private String nome;
    private Funcionario gerente;
    private Funcionario secretario;
    private ArrayList<Funcionario> operarios;

    public Departamento(String nome, Funcionario gerente, Funcionario secretario, ArrayList<Funcionario> operarios) {
        this.nome = nome;
        this.gerente = gerente;
        this.secretario = secretario;
        this.operarios = operarios;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getGerente() {
        return gerente.getName();
    }

    public void setGerente(Funcionario gerente) {
        this.gerente = gerente;
    }

    public String getSecretario() {
        return secretario.getName();
    }

    public void setSecretario(Funcionario secretario) {
        this.secretario = secretario;
    }

    public String returnEmpregados() {
        String x = "";
        String secretarioName = secretario.getName();
        String gerenteName = gerente.getName();
        for (Funcionario f : operarios) {
            x += "- " + f.getName() + "\n";

        }
        return "Operarios: \n" + x + "Secretario - " + secretarioName + "\nGerente - " + gerenteName;

    }

    public String getOperarios() {
        String x = "";
        for (Funcionario f : operarios) {
            x += "- " + f.getName() + "\n";

        }
        return x;
    }

    public String verifySalary() {
        double max = 0;
        double min = 999999;

        for (Funcionario f : operarios) {
            double atual = f.getSalary();
            if (atual > max) {
                max = atual;
            }
            if (atual < min) {
                min = atual;
            }
        }

        System.out.println("Min:" + min + "Max" + max);


        if (min >= (max * 0.8)) {
            return "Salario OK!";
        }

        return "Salario fora do intervalo";
    }

    public void setOperarios(ArrayList<Funcionario> operarios) {
        this.operarios = operarios;
    }

    public void cadastrarOperario(Funcionario f) {
        if (operarios.size() < 5) {
            operarios.add(f);
        } else {
            throw new Error("Deu bret - [Numero de operarios excedeu o limite]");
        }
    }

}
